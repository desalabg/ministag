#!/usr/bin/env python
# coding: utf-8

# Packages
import numpy as np
import matplotlib.pyplot as plt
import h5py
import argparse



def load_npz(filename, plot=False):
    npzfile = np.load(filename)
    T = npzfile['T']
    vx = npzfile['vx']
    vz = npzfile['vz']
    v = np.sqrt(vx**2+vz**2)
    p = npzfile['p']
    nx = np.shape(T)[0]
    nz = np.shape(T)[1]
    T_av = np.mean(T,0)

    export_dict = {
        "T":T,
        "vx":vx,
        "vz":vz,
        "v":np.sqrt(vx**2+vz**2),
        "p":p,
        "nx":np.shape(T)[0],
        "nz": np.shape(T)[1],
        "T_av":np.concatenate(([1],T_av,[0])),
        "vx_av":np.mean(vx,0),
        "vz_av":np.mean(vz,0),
        "v_av":np.mean(v,0),
        "p_av":np.mean(p,0),
        "z":np.linspace(0,1,nz+2),
        }
    return export_dict


def load_h5(filename, idir_name="."):
    f = h5py.File((idir_name + '/'+filename),'r')
    dset = f['series']
    export_dict ={
    'it':dset[:,0],
    't':dset[:,1],
    'Tmin':dset[:,2],
    'Tmean':dset[:,3],
    'Tmax':dset[:,4],
    'ekin':dset[:,5],
    'v_x_rms':dset[:,6],
    'v_z_rms':(np.sqrt(dset[:,5]) - dset[:,6]),
    'Nu_bot':dset[:,7],
    'Nu_top':dset[:,8],
    }
    return export_dict

#---------------------------------------------------------------------------------#
# Loading data
#---------------------------------------------------------------------------------#
def main():
    parser = argparse.ArgumentParser(description=("Ministag UI"))
    parser.add_argument("directory_name", type=str, default=None,
                          help="read the data from disk")
    parser.add_argument("-t", type=str, default=None,
                          help="read the data from disk")
    args = parser.parse_args()
    dir_name = args.directory_name
    if dir_name[:1] == "./":
        dir_name=dir_name[2:]
    print('./' + dir_name+ '/time.h5')
    f = h5py.File((dir_name + '/time.h5'),'r')
    if args.t:
        timestamp = args.t
        if len(timestamp) < 8:
            timestamp = (8-len(timestamp))*'0' + timestamp


    dset = f['series']
    it = dset[:,0]
    t = dset[:,1]
    Tmin = dset[:,2]
    Tmean = dset[:,3]
    Tmax = dset[:,4]
    ekin = dset[:,5]
    v_x_rms = dset[:,6]
    v_z_rms = np.sqrt(ekin) - v_x_rms
    Nu_bot = dset[:,7]
    Nu_top = dset[:,8]

#---------------------------------------------------------------------------------#
#  Heat flux - Nusselt number
#---------------------------------------------------------------------------------#

### Plotting Nusselt number as a function of iteration number
    plt.figure(1)
    plt.plot(it,Nu_bot,label='bottom $Nu$')
    plt.plot(it,Nu_top,'--',label='top $Nu$')
    plt.xlabel('iteration number')
    plt.ylabel('Nusselt $Nu$')
    plt.grid(True)
    plt.legend()
    plt.show()
#---------------------------------------------------------------------------------#
# Calculating the time-averaged Nusselt number
#---------------------------------------------------------------------------------#
# Choose the iteration number defining the beginning of statistically steady state
    breakpts = np.where(Tmean[1:]<=Tmean[:-1])[0]
    breakpts = breakpts[breakpts > 1000]
    print(len(breakpts))
    if len(breakpts) > 0:
        it0 = np.min(breakpts)
    else:
        print(" WARNING!  It looks like temperature equilibrium was not reached!")
        print(f" In {dir_name} with max timestep {len(t)}. Try running it again, but for longer.")
        it0 = len(t) - 500
    Nu_bot_mean = np.mean(Nu_bot[it0:])
    Nu_bot_stdv = np.std(Nu_bot[it0:])
    Nu_top_mean = np.mean(Nu_top[it0:])
    Nu_top_stdv = np.std(Nu_top[it0:])

    plt.figure(2)
    gd_min, gd_max = np.min(t[it0:]), np.max(t[it0:])
    plt.plot(t[it0:],Nu_bot[it0:], label="Calculated Nu_bot")
    plt.plot(t[it0:],Nu_top[it0:], label="Calculated Nu_top")
    plt.hlines(Nu_bot_mean,gd_min, gd_max, color="darkblue",linewidth=2, label="Estimate")
    plt.hlines(Nu_top_mean,gd_min, gd_max, color="saddlebrown", linewidth=2, label="Estimate")
    plt.hlines(Nu_bot_mean-Nu_bot_stdv,gd_min, gd_max,linewidth=2, color="darkblue", linestyles="dashed", label="lower lim estimate")
    plt.hlines(Nu_bot_mean+Nu_bot_stdv,gd_min, gd_max,linewidth=2, color="darkblue", linestyles="dashed", label="upper lim estimate")
    plt.hlines(Nu_top_mean-Nu_top_stdv,gd_min, gd_max,linewidth=2, color="saddlebrown", linestyles="dashed", label="lower lim estimate")
    plt.hlines(Nu_top_mean+Nu_top_stdv,gd_min, gd_max,linewidth=2, color="saddlebrown", linestyles="dashed", label="upper lim estimate")
    plt.xlabel("time")
    plt.ylabel("Nusselt")
    plt.show()
    
    plt.figure(3)
    plt.plot(t, ekin)
    plt.figure(4)
    plt.plot(t[it0:],ekin[it0:])
    plt.xlabel("Time")
    plt.ylabel("ekin")
    plt.show()

    plt.figure(5)
    plt.plot(t, Tmean)
    plt.figure(6)
    plt.plot(t[it0:], Tmean[it0:])
    plt.show()
#---------------------------------------------------------------------------------#
# Reading .npz files
#---------------------------------------------------------------------------------#


    if args.t:
        npz_values = load_npz('./'+dir_name+'/fields'+timestamp+'.npz')
        z = npz_values["z"]
        vx_av = npz_values["vx_av"]
        vz_av = npz_values["vz_av"]
        v_av = npz_values["v_av"]

        ### Temperature profile
        plt.figure(3)
        plt.plot(npz_values["T_av"], npz_values["z"])
        plt.xlabel('$\overline{T}$')
        plt.ylabel('$z$')
        plt.grid(True)

        
    ### Velocity profile
        plt.figure(4)
        plt.plot(vx_av,z[1:-1],label='$\overline{v_x}$')
        plt.plot(vz_av,z[1:-1],label='$\overline{v_z}$')
        plt.plot(v_av,z[1:-1],label='$\overline{v}$')

        plt.xlabel('$\overline{v}$')
        plt.ylabel('$z$')
        plt.grid(True)
        plt.legend()

        plt.show()
        plt.show()

if __name__ == '__main__':
    main()