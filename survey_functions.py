import numpy as np
import h5py
import toml
from MiniStag_timeseries_reader import load_npz, load_h5
import sys
import os

sys.path.append("/home/greg/geo_interne/") # Your directory goes here

def load_pars(parfile):
    pars = toml.load(parfile)
    return pars

def survey_directory_npz(var_name):
    # This is not safe to use, except for getting the #instantaneous average# value from an .npz file
    # e.g. "Finding average velocity in one .npz"
    # I will add 3d-matrix support later
    dirs = os.listdir("./")
    v_av = []
    pars = []
    print(dirs)
    for gdir in dirs:
        if gdir[:-3] != ".py" and gdir[:3] == "out":
            nvav = np.empty(0)
            f_i = 0
            for filename in os.listdir('./'+gdir+"/"):
                if filename[-4:] == ".npz":
                    nvav_o = np.asarray(load_npz("./"+gdir+"/"+filename)[var_name])
                    #print(gdir," ", f_i, " : ", np.shape(nvav_o))
                    nvav = np.append(nvav, np.mean(nvav_o))
                    f_i = f_i +1
                elif filename[-5:]==".toml":
                    pars.append(load_pars("./"+gdir+"/"+filename))             
        
            v_av.append(nvav)
    return pars, v_av

def survey_directory_h5(var_name, direc="./"):
    dirs = os.listdir(direc)
    v_av = []
    pars = []
    print(dirs)
    for gdir in dirs:
        if gdir[:-3] != ".py" and gdir[:3] == "out":
            nvav = np.empty(0)
            for filename in os.listdir(direc+gdir+"/"):
                if filename[-3:] == ".h5":
                    nvav = np.asarray(load_h5(direc+gdir+"/"+filename)[var_name])
                elif filename[-5:]==".toml":
                    pars.append(load_pars(direc+gdir+"/"+filename))             
            v_av.append(nvav)
    return pars, v_av