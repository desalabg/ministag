#!/bin/bash
k=1
for f in ./run_pars/*.toml; do
  ministag -i ./run_pars/par"$k".toml -o ./output"$k"
  convert -append output"$k"/*.png output"$k"/summary.png
  echo $k
  ((k++))
done
