"""Make ministag callable."""

import importlib
import pathlib
import shutil
import signal
import sys


def _sigint_handler(*_):
    """Handler of SIGINT signal."""
    print('\nYour will to stop me is staggering.')
    sys.exit()


def main():
    """Entry point."""
    signal.signal(signal.SIGINT, _sigint_handler)
    print(r"""
        {_}
        /=-'
  )____//
 _//---\|_
/         /
""")

    import argparse
    parser = argparse.ArgumentParser(description=("Ministag UI"))
    parser.add_argument("pars_toml", type=str, default=None,
                          help="read the data from disk")
    parser.add_argument("-o", type=str, default=None,
                        help="output directory")
    args = parser.parse_args()
    #par_fnm = args.i # /run_pars/
    par_fnm = args.pars_toml
   

    if par_fnm[-5:]==".toml" and (not args.o):
        fin_outdir = './output_'+par_fnm[:-5]
    elif args.o:
        fin_outdir = args.o
    else:
        print(" Could not recover file basename. Outputting to ./output")
        fin_outdir = './output'

    solver = importlib.import_module('ministag.solver')
    #par = pathlib.Path('par.toml')
    rb2d = solver.RayleighBenardStokes(outdir=fin_outdir, parfile=par_fnm) # par if par.is_file() else None)
    if not rb2d.restart and rb2d.outdir.is_dir():
        print('Output directory already exists.',
              'Resuming may lead to loss of data.')
        answer = input('Keep on going anyway (y/N)? ')
        if answer.lower() != 'y':
            sys.exit()
        shutil.rmtree(rb2d.outdir)
    rb2d.outdir.mkdir(exist_ok=True)
    print(par_fnm)
    par_fnm = par_fnm.replace("./run_pars/", "")
    rb2d.dump_pars(rb2d.outdir / par_fnm)
    rb2d.solve(progress=True)


if __name__ == '__main__':
    main()
